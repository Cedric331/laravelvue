<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'accueil')->name('accueil');

Route::get('/boutique', 'ProduitController@getAll');

Route::post('/AddProduit', 'ProduitController@store');



Route::get('/panier', 'PanierController@getAll');

Route::post('/panier', 'PanierController@store');

Route::delete('/Panier/{id}', 'PanierController@delete');




// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
