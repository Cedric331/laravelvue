/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
Vue.use(VueRouter);

import BoutiqueProduit from './components/Boutique/BoutiqueProduit'
import Panier from './components/Panier/Panier.vue'
import Produit from './components/Produit/Produit.vue'
import Erreur from './components/Erreur.vue'


Vue.component('navigation', require('./components/Navigation.vue').default);

const routes = [
        {path: "/", component:BoutiqueProduit},
        {path: "/boutique", component:BoutiqueProduit},
        {path: "/panier", component:Panier},
        {path: "/ajout-produit", component:Produit},
        {path: "/*", component:Erreur},
]

const router = new VueRouter({routes})

const app = new Vue({
    router:router,
    el: '#app',
});
