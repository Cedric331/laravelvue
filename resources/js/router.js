import Vue  from 'vue';
import VueRouter from 'vue-router'

Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [
        {path: "/", component:Navigation},
        {path: "/boutique", component:BoutiqueProduit},
        {path: "/panier", component:Panier},
        {path: "/description", component:Description},
        {path: "/accueil", component:Accueil},
        {path: "/ajout-produit", component:Produit},
        {path: "/*", component:Erreur},
    ]
})

