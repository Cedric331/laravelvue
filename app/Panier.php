<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panier extends Model
{

        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'panier_id'
    ];

    public function Produit()
    {
        return $this->belongsTo(Produit::class);
    }
}
