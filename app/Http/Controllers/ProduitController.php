<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produit;


class ProduitController extends Controller
{


    public function store(Request $request)
    {

        $produit = new Produit;

        $produit->titre = $request->titre;
        $produit->description = $request->description;
        $produit->image = $request->image;
        $produit->prix = $request->prix;

        $produit->save();
    }

    public function getAll()
    {
        $produits = Produit::all();
        return response()->json($produits);
    }
}
