<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Panier;

class PanierController extends Controller
{

    public function store(Request $request)
    {
        dd($request);

        $panier = new Panier;

        $panier->panier_id = $request->id;

        $panier->save();
    }

    public function getAll()
    {
        $panier = Panier::all();
        return response()->json($panier);
    }

    public function delete($id)
    {
        $panier = Panier::find($id);
        $panier->delete;
    }
}
